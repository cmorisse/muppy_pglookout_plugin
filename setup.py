import setuptools

setuptools.setup(
    name="muppy_pglookout_plugin",
    version="0.0.1",
    #author="Example Author",
    #author_email="author@example.com",
    #description="A small example package",
    #long_description="""long_description""",
    #long_description_content_type="text/markdown",
    #url="https://github.com/pypa/sampleproject",
    #packages=[setuptools.find_packages()],
    packages=['muppy_pglookout_plugin'],
    #classifiers=[
    #    "Programming Language :: Python :: 3",
    #    "License :: OSI Approved :: MIT License",
    #    "Operating System :: OS Independent",
    #],
    python_requires='>=3.6',
)