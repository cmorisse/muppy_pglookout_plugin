import datetime
import errno
import logging
import psycopg2
import requests
import select
import time
import json
import timeit
import importlib
from concurrent.futures import as_completed, ThreadPoolExecutor
from email.utils import parsedate
from psycopg2.extras import RealDictCursor
from queue import Empty
from threading import Thread

from pglookout import logutil
from pglookout.common import get_iso_timestamp, parse_iso_datetime, json_datetime_serializer
from pglookout.cluster_monitor import PglookoutTimeout, wait_select

def humanize(val):
    """Return val in bytes in human readable form."""
    units = ('bytes', 'Kb', 'Mb', 'Gb')
    l_size = val
    unit_idx =0
    while l_size >= 1024 and unit_idx < len(units)-1:
        l_size /= 1024
        unit_idx += 1
    return "%0.2f %s" % (l_size, units[unit_idx])


def dictfetchall(cr):
    """ Returns a dict from a RealDictCursor """
    return [{item[0]: item[1] for item in row.items()} for row in cr.fetchall()]

def user_standby_status_query(self, result, instance, db_conn):
    result['extended_replication_stats'] = {
        "cluster_replication_stats_timestamp": result['fetch_time']
    }
    # Extract recovery info then inject it into result
    if result['connection']:
        result['extended_replication_stats']['recovery_stats'] = {
            "query_stat_timestamp": result['fetch_time'],
            "query_stat_result": {
                "db_time": result['db_time'],
                "pg_is_in_recovery": result['pg_is_in_recovery'],
                "pg_last_xact_replay_timestamp": result['pg_last_xact_replay_timestamp'],
                "pg_last_wal_receive_lsn": result['pg_last_xlog_receive_location'],
                "pg_last_wal_replay_lsn": result['pg_last_xlog_replay_location'], 
                "pg_is_wal_replay_paused": result['pg_is_wal_replay_paused']
            }
        }
    else:
        result['extended_replication_stats']['recovery_stats'] = {
            "query_stat_timestamp": result['fetch_time'],
            "query_stat_result": {},
            "query_stat_error": "Connection failed!"
        }

    # Query server for replication and receiver stats
    result['extended_replication_stats']['replication_stats'] = {
        "query_stat_timestamp": get_iso_timestamp(),  # pglookout::fetch_time
        "query_stat_result": [],
        "query_stat_error": "Connection failed!"
    }        
    result['extended_replication_stats']['receiver_stats'] = {
        "query_stat_timestamp": get_iso_timestamp(),  # pglookout::fetch_time
        "query_stat_result": [],
        "query_stat_error": "Connection failed!"
    }        

    if not db_conn:
        db_conn = self._connect_to_db(instance, self.config["remote_conns"].get(instance))
        if not db_conn:
            return result
    try:
        phase = "querying pg_stat_replication from"
        self.log.debug("%s %s", phase, instance)
        c = db_conn.cursor(cursor_factory=RealDictCursor)
        c.execute("SELECT * FROM pg_stat_replication")
        wait_select(c.connection)
        q_result = dictfetchall(c)
        result['extended_replication_stats']['replication_stats']['query_stat_result'] = q_result
        del result['extended_replication_stats']['replication_stats']['query_stat_error']

        phase = "querying pg_stat_wal_receiver from"
        self.log.debug("%s %s", phase, instance)
        c = db_conn.cursor(cursor_factory=RealDictCursor)
        c.execute("SELECT * FROM pg_stat_wal_receiver;")
        wait_select(c.connection)
        q_result = dictfetchall(c)
        if len(q_result)>1:
            self.log.error("Multiple receivers for cluster:'%s'", instance)
        elif len(q_result)==1:
            q_result = q_result[0]                
        result['extended_replication_stats']['receiver_stats']['query_stat_result'] = q_result
        del result['extended_replication_stats']['receiver_stats']['query_stat_error']

    except (PglookoutTimeout, psycopg2.DatabaseError, psycopg2.InterfaceError, psycopg2.OperationalError) as ex:
        self.log.warning("%s (%s) %s %s", ex.__class__.__name__, str(ex).strip(), phase, instance)
        db_conn.close()
        self.db_conns[instance] = None
        # Return "no connection" result in case of any error. If we get an error for master server after the initial
        # query we'd end up returning completely invalid value for master's current position

    return

def broadcast_cluster_state(self):
    broadcast_url = self.config['state_broadcaster_plugin'].get('muppy_broadcast_url')
    static_token = self.config['state_broadcaster_plugin'].get('muppy_static_token')
    if broadcast_url:
        start_ts = timeit.default_timer()
        resp = requests.post(broadcast_url,
                             data=json.dumps({'params': self.cluster_state}, default=json_datetime_serializer),
                             headers={'Content-Type': 'application/json', 'muppy-static-token': static_token},
                             timeout=2)
        end_ts = timeit.default_timer()
        if resp.status_code in (200,):
            self.log.info("cluster stats sent to '%s' in %.3fms", broadcast_url, (end_ts-start_ts)*1000)
        else:
            self.log.warning("Failed to send cluster stats to '%s'.", broadcast_url)
    